#!/bin/bash -x
set -euo pipefail
IFS=$'\n\t'

LOCAL_CONTAINER=$(docker container list | grep symposion_app | head -1 | cut -f 1 -d \ )


for FIXTURE_FILE in $*; do
    FIXTURE=${FIXTURE_FILE##fixtures/}
    docker cp fixtures/${FIXTURE} ${LOCAL_CONTAINER}:/tmp
    docker exec -it ${LOCAL_CONTAINER} ./manage.py loaddata /tmp/${FIXTURE}
done

