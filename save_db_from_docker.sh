#!/bin/bash -x
source common.sh
set -euo pipefail
IFS=$'\n\t'

docker cp ${LOCAL_CONTAINER}:/tmp/symposion.sqlite ${BACKUP_DIR}/${TIMESTAMP}.sqlite
