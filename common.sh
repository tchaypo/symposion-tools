DEFAULT_DJANGO_APPS="auth symposion_speakers symposion_proposals contenttypes proposals registrasion sessions sites sitetree symposion symposion_conference symposion_reviews teams"
DJANGO_APPS=${1:-${DEFAULT_DJANGO_APPS}}
EXCLUDE_APPS_LIST="symposion_reviews.resultnotification symposion_proposals.supportingdocument registrasion.cart registrasion.attendee registrasion.attendeeprofilebase registrasion.invoice registrasion.lineitem registrasion.paymentbase"
EXCLUDE_APPS=""
for APP in ${EXCLUDE_APPS_LIST}; do
    EXCLUDE_APPS="${EXCLUDE_APPS} --exclude ${APP}"
done
PROD_POD=$(kubectl get --namespace rego-prod pods | grep symposion-app | tail -1 | cut -f 1 -d \ )
DEV_POD=$(kubectl get --namespace rego-dev pods | tail -1 | cut -f 1 -d \ )

LOCAL_CONTAINER=$(docker container list | grep symposion_app | head -1 | cut -f 1 -d \ )

TIMESTAMP=${TIMESTAMP:-$(date -u +"%Y-%m-%dT%H%M%SZ")}
BACKUP_DIR=sql-backups
mkdir -p ${BACKUP_DIR}

alias prod="kubectl exec --namespace rego-prod -it ${PROD_POD} --"
alias dev="kubectl exec --namespace rego-dev -it ${DEV_POD} --"
alias sdock="docker exec -it ${LOCAL_CONTAINER}"

export prod dev sdock
