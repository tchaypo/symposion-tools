#!/bin/bash -x
source common.sh
set -euo pipefail
IFS=$'\n\t'

BACKUP=${1##sql-backups/}
BACKUP=${BACKUP%%.sqlite}

docker cp ${BACKUP_DIR}/${BACKUP}.sqlite ${LOCAL_CONTAINER}:/tmp/symposion.sqlite
