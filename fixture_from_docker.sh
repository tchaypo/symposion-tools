#!/bin/bash -x
source common.sh
set -euo pipefail
IFS=$'\n\t'

DJANGO_APPS="${1:-auth	symposion_conference	flatpages	symposion_proposals	teams	symposion_speakers	symposion_schedule}"

for app in ${DJANGO_APPS}; do
    OUTPUT_FILE=fixtures/dev-${app}.json
    docker exec -it ${LOCAL_CONTAINER} ./manage.py dumpdata --natural-foreign --natural-primary ${app} | tail -1 | python -m json.tool > ${OUTPUT_FILE}
done

